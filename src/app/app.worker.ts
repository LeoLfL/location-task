/// <reference lib="webworker" />

import {
  WorkerData,
  MessageFromWorker,
  WorkerResult
} from './app.worker.model';
import * as GEO from './utils/distance.util';
import { delay } from './utils/util';

addEventListener('message', async ({ data }: { data: WorkerData }) => {
  const fileReader = new FileReader();
  fileReader.onload = event => onFileLoaded(event, data);
  fileReader.readAsText(data.file, 'UTF-8');
});

const onFileLoaded = async (fileLoadedEvent: any, data: WorkerData) => {
  const textFromFileLoaded = fileLoadedEvent.target.result;
  let circleData = data.circleData;

  let circleCentrLat = circleData.centerOfRadius.lat;
  let circleCentrLon = circleData.centerOfRadius.lon;

  let circleRadius = GEO.getDistanceFromLatLonInKm(
    circleCentrLat,
    circleCentrLon,
    circleData.borderOfRadius.lat,
    circleData.borderOfRadius.lon
  );

  postMessage({
    type: 'calculated_radius',
    value: circleRadius
  } as MessageFromWorker);

  let parsedCSV = GEO.parseCSV(textFromFileLoaded);

  let results: WorkerResult[] = [];
  for (let i = 0; i < parsedCSV.length; i++) {
    let latLon = parsedCSV[i];

    let latFromFile = Number(latLon[0].trim());
    let lonFromFile = Number(latLon[1].trim());

    let distanceBetweenPoints = GEO.getDistanceFromLatLonInKm(
      circleCentrLat,
      circleCentrLon,
      latFromFile,
      lonFromFile
    );

    results.push({
      lat: latFromFile,
      lon: lonFromFile,
      matched: distanceBetweenPoints < circleRadius,
      distanceInKm: distanceBetweenPoints
    });

    if ((i % 10 === 0 && i !== 0) || i === parsedCSV.length - 1) {
      postMessage({
        type: 'progress',
        value: Math.floor((i / (parsedCSV.length - 1)) * 100)
      } as MessageFromWorker);
      postMessage({ type: 'results', value: results } as MessageFromWorker);
      results = [];
    }

    // just for the test purpose
    await delay(10);
  }
};
