### Single-page application (with Worker) which calculate the number of locations from the uploaded csv which are located within the cirsle radius

The CSV file path is examples/addresses.csv

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.3.

The project uses a Worker, if available, or simply calculates the result if not.

The project avoids freezing the browser (because the heavy calculation) in both cases (Worker and normal calculation)

### Development server

Run `ng serve` for a dev server with hmr. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Possible improvements
- add additional components and move the logic from the app.component.ts
- find the better parser for specific requirements 
- add logic for avoiding the freezing of the browser when doing GEO.parseCSV (valid only for fallback case, because we don't care about calculation inside worker)
- find the reason why some services returns a little bit different answer when we calculate distance between two lan/lot points (getDistanceFromLatLonInKm)
- add better validation logic and try to avoid duplicate validation code / formControl
- add infinity scroll for table results
- move worker and it types to the separate folder 
